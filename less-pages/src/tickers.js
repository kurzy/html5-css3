export const randomTicker = ({
  container, min = 0, max = 100, interval = 500, round = false,
}) => {
  if (!container) {
    throw new Error('Missing container');
  }
  setInterval(() => {
    const value = Math.random() * (max - min) + min;
    const event = new CustomEvent('randomTickerChange', { detail: { value: round ? Math.round(value) : value } });
    container.dispatchEvent(event);
  }, interval);
};

export const loaderTicker = ({
  container, min = 0, max = 100, step = 1, interval = 500,
}) => {
  if (!container) {
    throw new Error('Missing container');
  }

  let value = min;
  setInterval(() => {
    const event = new CustomEvent('loaderTickerChange', { detail: { value } });
    container.dispatchEvent(event);

    value += step;
    if (value > max) {
      value = 0;
    }
  }, interval);
};
