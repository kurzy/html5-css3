const CopyPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');

const pages = [
  'index',
];

const plugins = [
  new MiniCssExtractPlugin({
    filename: 'css/[name].css',
  }),
  new CopyPlugin({
    patterns: [
      { from: "./public", to: "./" },
    ],
  }),
];

plugins.push(
  ...pages.map(
    (page) =>
      new HtmlWebpackPlugin({
        inject: true,
        scriptLoading: 'defer',
        template: `./pages/${page}.html`,
        filename: `${page}.html`,
        chunks: [page],
      }),
  ),
);

module.exports = {
  entry: pages.reduce((config, page) => {
    config[page] = path.resolve(__dirname, 'src', `${page}.js`);
    return config;
  }, {}),
  output: {
    filename: 'scripts/[name].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
  plugins,
};
