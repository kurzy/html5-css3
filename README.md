# HTML5 a CSS3 pro pokročilé

[Repozitář](https://bit.ly/html5css3-repo)

## Nevidíte data z vašeho kurzu?
- Najděte rozbalovací nabídku kousek nahoře vlevo nad seznamem souborů, ve které vidíte slovo „main“ (vedle ní je text „html5-css3“).
- Klikněte na ni.
- V sekci Tags klikněte na datum Vašeho kurzu.
