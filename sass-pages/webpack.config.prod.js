const { merge } = require('webpack-merge');
const common = require('./webpack.config.common.js');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");

const rules = [
  {
    test: /\.(s(a|c)ss)$/u,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
      },
      {
        loader: 'css-loader',
        options: {
          sourceMap: true,
        },
      },
      {
        loader: "postcss-loader",
        options: {
          postcssOptions: {
            plugins: [
              [
                "postcss-preset-env",
                {},
              ],
            ],
          },
        },
      },
      {
        loader: 'sass-loader',
        options: {
          implementation: require("node-sass"),
        }
      },
    ],
  },
];

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  devServer: undefined,
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin(), new CssMinimizerPlugin()],
    splitChunks: {
      chunks: 'all',
    },
  },
  performance: {
    maxAssetSize: 270000,
  },
  module: {
    rules,
  },
});
