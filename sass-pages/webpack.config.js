const { merge } = require('webpack-merge');
const common = require('./webpack.config.common.js');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');

const rules = [
  {
    test: /\.(s(a|c)ss)$/u,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
      },
      {
        loader: 'css-loader',
        options: {
          sourceMap: true,
        },
      },
      {
        loader: 'sass-loader',
        options: {
          implementation: require("sass"),
        }
      },
    ],
  },
];

module.exports = merge(common, {
  mode: 'development',
  devtool: 'eval',
  devServer: {
    port: 9000,
    open: true,
    static: {
      directory: path.join(__dirname, './dist'),
    },
  },
  module: {
    rules,
  }
});
