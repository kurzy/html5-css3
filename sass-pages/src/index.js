import { loaderTicker, randomTicker } from './tickers';
import './sass/index.scss';
import { getRangeValue, handleSubmit } from './form';

const meter = document.querySelector('#cofee');
if (meter) {
  randomTicker({
    container: meter,
    min: 70,
    max: 100,
    round: true,
    interval: 1000,
  });
  const { parentNode } = meter;
  const span = document.createElement('span');
  parentNode.appendChild(span);
  meter.addEventListener('randomTickerChange', (event) => {
    const { detail: { value } } = event;
    const degrees = `${value} °C`;
    meter.textContent = degrees;
    meter.value = value;
    meter.title = degrees;
    span.textContent = degrees;
  });
}
const loader = document.querySelector('#deploy');
if (loader) {
  loaderTicker({
    container: loader,
    interval: 125,
  });

  const { parentNode } = loader;
  const span = document.createElement('span');
  parentNode.appendChild(span);

  loader.addEventListener('loaderTickerChange', (event) => {
    const { detail: { value } } = event;
    const loadString = `${value} %`;
    span.textContent = loadString;

    loader.value = value;
    loader.textContent = loadString;
  })
}

getRangeValue();
handleSubmit();
