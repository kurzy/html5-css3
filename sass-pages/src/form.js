//range value

const createMinutes = (value) => `${value} min.`;

export const getRangeValue = () => {
  const range = document.querySelector("#length");
  const output = document.querySelector("#range-output");
  if (!range || !output) {
    return;
  }

  output.textContent = createMinutes(range.value);
  range.addEventListener('input', () => {
    output.textContent = createMinutes(range.value);
  });
};

export const handleSubmit = () => {
  const form = document.querySelector("#questions");
  const output = document.querySelector("#range-output");
  if (!form || !output) {
    return;
  }

  form.addEventListener('submit', (event) => {
    event.preventDefault();
    const formData = new FormData(form);
    const searchParams = new URLSearchParams(formData);
    const queryString = searchParams.toString();

    fetch(`https://jsdev.cz/xhr/?${queryString}`)
      .then((response) => response.text())
      .then((data) => output.textContent = data);
  });
};
